package com.atlassian.labs.fedex19.blame;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class StorageFormatTokens extends DefaultHandler
{
    private List<SFThing> things = new ArrayList<SFThing>();

    private static SFThing START = SFThing.ofStartElement("root"),
            END = SFThing.ofEndElement("root");

    public List<SFThing> parse(String s) throws ParserConfigurationException, SAXException, IOException
    {
        String doc = "<?xml version='1.0'?>"
                + "<!DOCTYPE root ["
                + "<!ENTITY nbsp   \"&#160;\">"
                + "]>"
                + "<root>"
                + s + "</root>";

        SAXParser sp = SAXParserFactory.newInstance().newSAXParser();

        sp.parse(new InputSource(new StringReader(doc)), this);

        return getContents();
    }

    List<SFThing> getContents()
    {
        if (things.size() < 2)
        {
            throw new IllegalStateException();
        }

        if (!things.get(0).equals(START))
        {
            throw new IllegalStateException("Bad start");
        }

        if (!things.get(things.size() - 1).equals(END))
        {
            throw new IllegalStateException("Bad end");
        }

        return things.subList(1, things.size() - 1);
    }

    Iterable<String> split(String text)
    {
        return Collections.singleton(text);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        for (String s : split(new String(ch, start, length)))
        {
            things.add(SFThing.ofText(s));
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        Map<String, String> attrs = new HashMap<String, String>();

        for (int i = 0; i < attributes.getLength(); i++)
        {
            attrs.put(attributes.getQName(i), attributes.getValue(i));
        }

        things.add(SFThing.ofStartElement(qName, attrs));
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        things.add(SFThing.ofEndElement(qName));
    }

    static final String XHTML_NS = "http://www.w3.org/1999/xhtml";

    public static StorageFormatTokens byWordFragments()
    {
        return new StorageFormatTokens() {
            @Override
            Iterable<String> split(String text)
            {
                List<String> fragments = new ArrayList<String>(Arrays.asList(text.split("\\b")));
                Iterator<String> i = fragments.iterator();
                while (i.hasNext())
                {
                    if (i.next().isEmpty())
                    {
                        i.remove();
                    }
                }

                return fragments;
            }
        };
    }

    public static StorageFormatTokens byCharacter()
    {
        return new StorageFormatTokens() {
            @Override
            Iterable<String> split(String text)
            {
                List<String> chars = new ArrayList<String>();

                for (char c : text.toCharArray())
                {
                    chars.add(Character.toString(c));
                }

                return chars;
            }
        };
    }
}
