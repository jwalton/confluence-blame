package com.atlassian.labs.fedex19.blame;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class StorageFormatTokensTest
{
    @Test
    public void emptyDocumentToTokens() throws Exception
    {
        assertEquals(Collections.emptyList(), new StorageFormatTokens().parse(""));
    }

    @Test
    public void justTextDocumentToTokens() throws Exception
    {
        assertEquals(Collections.singletonList(SFThing.ofText("text")),
                new StorageFormatTokens().parse("text"));
    }

    @Test
    public void documentWithMarkupToTokens() throws Exception
    {
        String storageFormat = "<p>This is a <strong>test</strong>.</p>";

        List<SFThing> expected = Arrays.asList(
                SFThing.ofStartElement("p"),
                SFThing.ofText("This is a "),
                SFThing.ofStartElement("strong"),
                SFThing.ofText("test"),
                SFThing.ofEndElement("strong"),
                SFThing.ofText("."),
                SFThing.ofEndElement("p"));

        assertEquals(expected,
                new StorageFormatTokens().parse(storageFormat));
    }

    @Test
    public void nbspIsParsed() throws Exception
    {
        assertEquals(Collections.singletonList(SFThing.ofText("\u00A0")),
                new StorageFormatTokens().parse("&nbsp;"));
    }

    @Test
    public void splittingByWordFragments() throws Exception
    {
        StorageFormatTokens sft = StorageFormatTokens.byWordFragments();

        assertEquals(Arrays.asList(
                SFThing.ofText("This"),
                SFThing.ofText(" "),
                SFThing.ofText("isn"),
                SFThing.ofText("'"),
                SFThing.ofText("t"),
                SFThing.ofText(" "),
                SFThing.ofText("a"),
                SFThing.ofText(" "),
                SFThing.ofText("test"),
                SFThing.ofText(".")
                ),
                sft.parse("This isn't a test."));
    }

    @Test
    public void splittingIntoSeparateCharacters() throws Exception
    {
        StorageFormatTokens sft = StorageFormatTokens.byCharacter();

        assertEquals(Arrays.asList(
                SFThing.ofText("T"),
                SFThing.ofText("e"),
                SFThing.ofText("s"),
                SFThing.ofText("t")
                ),
                sft.parse("Test"));
    }

    @Test
    public void parsingContentWithNamespaces() throws Exception
    {
        StorageFormatTokens sft = new StorageFormatTokens();

        List<SFThing> expected = Arrays.asList(
                SFThing.ofStartElement("ac:link"),
                SFThing.ofStartElement("ri:user"),
                SFThing.ofEndElement("ri:user"),
                SFThing.ofEndElement("ac:link"));

        assertEquals(expected,
                sft.parse("<ac:link><ri:user /></ac:link>"));
    }

    @Test
    public void elementsStartsWithDifferentAttributesAreDifferent()
    {
        SFThing a = SFThing.ofStartElement("ri:user", Collections.singletonMap("ri:username", "user"));
        SFThing b = SFThing.ofStartElement("ri:user");

        assertFalse(a.equals(b));
    }

    @Test
    public void parsingContentWithAttributes() throws Exception
    {
        StorageFormatTokens sft = new StorageFormatTokens();

        List<SFThing> expected = Arrays.asList(
                SFThing.ofStartElement("ac:link"),
                SFThing.ofStartElement("ri:user", Collections.singletonMap("ri:username", "user")),
                SFThing.ofEndElement("ri:user"),
                SFThing.ofEndElement("ac:link"));

        assertEquals(expected,
                sft.parse("<ac:link><ri:user ri:username=\"user\" /></ac:link>"));
    }
}
