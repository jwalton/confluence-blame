package com.atlassian.labs.fedex19.blame;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.megginson.sax.XMLWriter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


public class OutputPage
{
    private final XMLWriter xw;

    private final boolean asHtml;

    public OutputPage(XMLWriter xw)
    {
        this(xw, false);
    }

    public OutputPage(XMLWriter xw, boolean asHtml)
    {
        this.xw = xw;
        this.asHtml = asHtml;
    }

    Map<Revision<SFThing>, String> colours = new TreeMap<Revision<SFThing>, String>();

    String colourFor(Revision<SFThing> r)
    {
        String colour = colours.get(r);
        if (colour == null)
        {
            colour = randomColour(r);
            colours.put(r, colour);
        }
        return colour;
    }

    Attributes attsForRevision(Revision<SFThing> r)
    {
        AttributesImpl atts = new AttributesImpl();
        atts.addAttribute("", "style", "style", "CDATA", "background-color: " + colourFor(r) + ";");
        atts.addAttribute("", "title", "title", "CDATA", titleFor(r));
        return atts;
    }

    public String titleFor(Revision<SFThing> r)
    {
        return "Revision: " + r.toString();
    }

    public String randomColour(Revision<SFThing> r)
    {
        long c = (long) (Math.random() * 0xffffff) | 0x808080;
        return String.format("#%06x", c);
    }

    public String getRevLabel(Revision<SFThing> r)
    {
        return r.toString();
    }

    public void writeRevisionKey(XMLWriter xw) throws SAXException
    {
        xw.startElement(StorageFormatTokens.XHTML_NS, "p");
        for (Map.Entry<Revision<SFThing>, String> e : colours.entrySet())
        {
            xw.startElement(StorageFormatTokens.XHTML_NS, "span", "span", attsForRevision(e.getKey()));
            xw.characters(getRevLabel(e.getKey()));
            xw.endElement(StorageFormatTokens.XHTML_NS, "span");
        }
        xw.endElement(StorageFormatTokens.XHTML_NS, "p");
    }

    public void writeBlamed(List<Blame<SFThing>> blame) throws SAXException
    {
        Revision<SFThing> inRev = null;

        for (Blame<SFThing> b : blame)
        {
            SFThing t = b.getContent();

            if (t instanceof SFTextThing)
            {
                if (inRev != b.getRevision())
                {
                    if (inRev != null)
                    {
                        xw.endElement(StorageFormatTokens.XHTML_NS, "span");
                    }
                    xw.startElement(StorageFormatTokens.XHTML_NS, "span", "span", attsForRevision(b.getRevision()));
                    inRev = b.getRevision();
                }
//                xw.startElement(StorageFormatTokens.XHTML_NS, "span", "span", attsForRevision(b.getRevision()));
//                xw.characters("{");
                t.write(xw);
//                xw.characters("}");
//                xw.endElement(StorageFormatTokens.XHTML_NS, "span");
            }
            else if (t instanceof SFStartElement)
            {
                if (inRev != null)
                {
                    xw.endElement(StorageFormatTokens.XHTML_NS, "span");
                    inRev = null;
                }
                t.write(xw);

                if (asHtml)
                {
                    xw.startElement(StorageFormatTokens.XHTML_NS, "span", "span", attsForRevision(b.getRevision()));
                    xw.characters("(");
                    xw.endElement(StorageFormatTokens.XHTML_NS, "span");
                }
            }
            else if (t instanceof SFEndElement)
            {
                if (inRev != null)
                {
                    xw.endElement(StorageFormatTokens.XHTML_NS, "span");
                    inRev = null;
                }

                if (asHtml)
                {
                    xw.startElement(StorageFormatTokens.XHTML_NS, "span", "span", attsForRevision(b.getRevision()));
                    xw.characters(")");
                    xw.endElement(StorageFormatTokens.XHTML_NS, "span");
                }

                t.write(xw);
            }
        }

        if (inRev != null)
        {
            xw.endElement(StorageFormatTokens.XHTML_NS, "span");
            inRev = null;
        }
    }

    public void start() throws SAXException
    {
        xw.startDocument();
        xw.setPrefix(StorageFormatTokens.XHTML_NS, "");
    }

    public void startHtmlBody() throws SAXException
    {
        start();

        xw.startElement(StorageFormatTokens.XHTML_NS, "html");
        xw.startElement(StorageFormatTokens.XHTML_NS, "body");
    }

    public void endHtmlBody() throws SAXException
    {
        xw.endElement(StorageFormatTokens.XHTML_NS, "body");
        xw.endElement(StorageFormatTokens.XHTML_NS, "html");
        xw.endDocument();
    }
}
