package com.atlassian.labs.fedex19.blame;

import java.util.Date;

public class RevDetails
{
    public Date when;
    public String who;
    public String revLabel;

    public String toString()
    {
        return who + ", " + when;
    }
}
