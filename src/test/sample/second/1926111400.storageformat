
<p>Git has plenty of support to <a href="http://git-scm.com/">get started</a> (as well as <a href="http://tartley.com/?p=1267">a useful spatial analogy</a>). This is a list of specific things to try to highlight the benefits and warm up to using it in production.</p>
<h3>Get started</h3>
<ol>
<li>Clone Crowd from the Git repository</li>
<li>Make another local clone from the first local one</li></ol>
<h3>Git stores whole history</h3>
<ol>
<li>How many commits led up to the 2.2.0 release?</li>
<li>How many different people are reponsible for the current contents of the root <code>pom.xml</code>?</li>
<li>What changes went into the 2.1.1 release?</li></ol>
<h3>Git has cheap local branching</h3>
<ol>
<li>Create a local branch</li>
<li>Make a commit to it</li>
<li>Go back to the master branch and use&nbsp;<code>gitk</code> to see your new branch</li>
<li>Delete the new branch</li></ol>
<h3>Git has easy local undo</h3>
<ol>
<li>Commit a change</li>
<li>Without pushing, reset it</li>
<li>Examine your undone commit in the Git repo</li></ol>
<h3>Git is distributed</h3>
<ol>
<li>Commit a change</li>
<li>Get it to another developer&nbsp;<strong>without</strong> pushing it back to the main repository and add it to the table:</li></ol>
<table>
<tbody>
<tr>
<th>Who?</th>
<th>To whom?</th>
<th>Method</th></tr>
<tr>
<td><ac:link><ri:user ri:username="jwalton" /></ac:link></td>
<td><ac:link><ri:user ri:username="psongsiritat" /></ac:link>​</td>
<td><code>git daemon</code>,&nbsp;<code>git pull</code> between workstation​s</td></tr>
<tr>
<td colspan="1"><ac:link><ri:user ri:username="jawong" /></ac:link></td>
<td colspan="1"><ac:link><ri:user ri:username="onevalainen" /></ac:link></td>
<td colspan="1"><code>git daemon</code>, <code>git pull</code> between workstations</td></tr>
<tr>
<td colspan="1"><ac:link><ri:user ri:username="onevalainen" /></ac:link></td>
<td colspan="1"><ac:link><ri:user ri:username="jawong" /></ac:link></td>
<td colspan="1">
<p><code>git daemon</code>, <code>git pull</code> between workstations</p></td></tr></tbody></table>
<h3>Git has multiple branches on the repository</h3>
<ol>
<li>Create a personal branch including your username</li>
<li>Commit an opinionated change</li>
<li>Push it to a new branch in the repository</li></ol>
<h3>Git is fast</h3>
<p>Make an unscientific comparison with Subversion and add it to the table:</p>
<table>
<tbody>
<tr>
<th>Operation</th>
<th>Svn</th>
<th>Git</th></tr>
<tr>
<td>​log | wc -l</td>
<td>​8.5s</td>
<td>0.06s​</td></tr>
<tr>
<td colspan="1">diff 2.2.0 2.2.8</td>
<td colspan="1">7.3s</td>
<td colspan="1">0.0.7s</td></tr>
<tr>
<td colspan="1">blame pom.xml</td>
<td colspan="1">4.9s</td>
<td colspan="1">1.4s</td></tr>
<tr>
<td colspan="1">checkout/clone</td>
<td colspan="1">11.2s</td>
<td colspan="1">90.3s (hey, you asked for unscientific comparison <ac:emoticon ac:name="smile" />)</td></tr></tbody></table>