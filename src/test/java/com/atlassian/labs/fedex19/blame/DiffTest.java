package com.atlassian.labs.fedex19.blame;
import java.util.Collections;

import org.incava.util.diff.Diff;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DiffTest
{
    @Test
    public void emptyIsSameAsEmpty()
    {
        Diff<Object> d = new Diff<Object>(Collections.emptyList(), Collections.emptyList());
        assertEquals(Collections.emptyList(), d.diff());
    }

    @Test
    public void nonEmptyButSameHasNoDifferences()
    {
        Diff<String> d = new Diff<String>(Collections.singletonList("Test"), Collections.singletonList("Test"));
        assertEquals(Collections.emptyList(), d.diff());
    }
}
