import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.labs.fedex19.blame.Blame;
import com.atlassian.labs.fedex19.blame.Blamer;
import com.atlassian.labs.fedex19.blame.OutputPage;
import com.atlassian.labs.fedex19.blame.Revision;
import com.atlassian.labs.fedex19.blame.SFThing;
import com.atlassian.labs.fedex19.blame.StorageFormatTokens;

import com.megginson.sax.XMLWriter;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

public class CmdLineBlamer
{
    public static void main(String[] args) throws Exception
    {
        List<File> files;

        if (args.length == 0)
        {
            files = Arrays.asList(new File("src/test/sample/first").listFiles());
            Collections.sort(files);
//            files = files.subList(1, 3);
        }
        else
        {
            files = new ArrayList<File>();
            for (String s : args)
            {
                files.add(new File(s));
            }
        }

//        blameAsListsOfLines(files);
        new CmdLineBlamer().blameAsSfThings(files);
    }

    static void blameAsListsOfLines(Iterable<File> files) throws IOException
    {
        List<Revision<String>> revisions = new ArrayList<Revision<String>>();

        for (File f : files)
        {
            revisions.add(new Revision<String>(FileUtils.readLines(f)));
        }

        for (Blame<String> b : Blamer.blame(revisions))
        {
            System.out.println(b.getRevision() + ": " + b.getContent());
        }
    }

    void blameAsSfThings(Iterable<File> files) throws IOException, ParserConfigurationException, SAXException
    {
        List<Revision<SFThing>> revisions = new ArrayList<Revision<SFThing>>();

        for (File f : files)
        {

            StorageFormatTokens sft = StorageFormatTokens.byWordFragments();

            revisions.add(new Revision<SFThing>(
                    sft.parse(FileUtils.readFileToString(f))));
        }

        for (Blame<SFThing> b : Blamer.blame(revisions))
        {
            System.out.println(b.getRevision() + ": " + b.getContent());
        }

        Writer w;

        w = new FileWriter("/tmp/a.xhtml");
//        w = new OutputStreamWriter(System.out);

        XMLWriter xw = new XMLWriter(w);

        OutputPage op = new OutputPage(xw);

        op.startHtmlBody();
        op.writeBlamed(Blamer.blame(revisions));
        op.writeRevisionKey(xw);
        op.endHtmlBody();

        w.close();
    }
}
