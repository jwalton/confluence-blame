import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Random;

import com.atlassian.labs.fedex19.blame.Blamer;
import com.atlassian.labs.fedex19.blame.ConfluenceHistory;
import com.atlassian.labs.fedex19.blame.OutputPage;
import com.atlassian.labs.fedex19.blame.Revision;
import com.atlassian.labs.fedex19.blame.SFThing;

import com.megginson.sax.XMLWriter;


public class ShowBlamed
{
    public static void main(String[] args) throws Exception
    {
        final ConfluenceHistory ch = ConfluenceHistory.load(new File("src/test/sample/second/index.csv"));

        Writer w;

        w = new FileWriter("/tmp/a.xhtml");
//        w = new OutputStreamWriter(System.out);

        XMLWriter xw = new XMLWriter(w);

        OutputPage op = new OutputPage(xw){
            @Override
            public String titleFor(Revision<SFThing> r)
            {
                return ch.revDetails.get(r).toString();
            }

            @Override
            public String randomColour(Revision<SFThing> r)
            {
                float hue = (float) Math.random();
                hue = new Random(ch.revDetails.get(r).who.hashCode()).nextFloat() + (float) (Math.random() / 5);
                float saturation = 0.2f + (float) (Math.random() / 5);
                float brightness = 0.85f;

                return String.format("#%06X", 0xFFFFFF & Color.HSBtoRGB(hue, saturation, brightness));
            }

            @Override
            public String getRevLabel(Revision<SFThing> r)
            {
                return ch.revDetails.get(r).revLabel;
            }
        };

        op.startHtmlBody();
        op.writeBlamed(Blamer.blame(ch.revisions));
        op.writeRevisionKey(xw);
        op.endHtmlBody();

        w.close();
    }
}
