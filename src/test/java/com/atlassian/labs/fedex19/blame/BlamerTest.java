package com.atlassian.labs.fedex19.blame;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.incava.util.diff.Diff;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


public class BlamerTest
{
    @Test
    public void blameNothing()
    {
        assertEquals(Collections.emptyList(), Blamer.blame(Collections.<Revision<String>>emptyList()));
    }

    @Test
    public void singleRevisionHistoryIsAllDueToThatChange()
    {
        Revision<String> rev = new Revision<String>(Arrays.asList("some", "content"));

        List<Revision<String>> history = Collections.singletonList(rev);

        List<Blame<String>> blame = Blamer.blame(history);
        assertEquals(2, blame.size());
        assertEquals("some", blame.get(0).getContent());
        assertEquals(rev, blame.get(0).getRevision());
        assertEquals("content", blame.get(1).getContent());
        assertEquals(rev, blame.get(1).getRevision());
    }

    @Test
    public void twoRevisionHistoryGetsAssignedCorrectly()
    {
        Revision<String> rev1 = new Revision<String>(Arrays.asList("content"));
        Revision<String> rev2 = new Revision<String>(Arrays.asList("new", "content"));

        List<Revision<String>> history = Arrays.asList(rev1, rev2);

        List<Blame<String>> blame = Blamer.blame(history);
        assertEquals(2, blame.size());
        assertEquals("new", blame.get(0).getContent());
        assertEquals(rev2, blame.get(0).getRevision());
        assertEquals("content", blame.get(1).getContent());
        assertEquals(rev1, blame.get(1).getRevision());
    }

    @Test
    public void moreComplicatedTwoRevisionHistoryGetsAssignedCorrectly()
    {
        Revision<String> rev1 = new Revision<String>(Arrays.asList("this", "is", "original", "content"));
        Revision<String> rev2 = new Revision<String>(Arrays.asList("and", "this", "is", "new", "content"));

        List<Revision<String>> history = Arrays.asList(rev1, rev2);

        List<Blame<String>> expected = Arrays.asList(
                new Blame<String>("and", rev2),
                new Blame<String>("this", rev1),
                new Blame<String>("is", rev1),
                new Blame<String>("new", rev2),
                new Blame<String>("content", rev1));

        List<Blame<String>> blame = Blamer.blame(history);
        assertEquals(5, blame.size());

        assertEquals(expected, blame);
    }

    @Test
    public void simpleThreeRevisionHistoryGetsAssignedCorrectly()
    {
        Revision<String> rev1 = new Revision<String>(Arrays.asList("a"));
        Revision<String> rev2 = new Revision<String>(Arrays.asList("a", "b"));
        Revision<String> rev3 = new Revision<String>(Arrays.asList("a", "b", "c"));

        List<Revision<String>> history = Arrays.asList(rev1, rev2, rev3);

        List<Blame<String>> expected = Arrays.asList(
                new Blame<String>("a", rev1),
                new Blame<String>("b", rev2),
                new Blame<String>("c", rev3));

        List<Blame<String>> blame = Blamer.blame(history);
        assertEquals(expected, blame);
    }

    @Test
    public void calculateLineOffsets()
    {
        Diff<String> diff = new Diff<String>(
                Arrays.asList("Q", "A", "B", "C",      "Z", "D"),
                Arrays.asList("Q",      "B", "C", "X", "Z", "E"));

        assertEquals(3, diff.diff().size());

        int[] expected = {
                0, -1, 1, 2, 4, -1
        };

        int[] previousLines = Blamer.calculatePreviousLines(6, diff.diff());
        assertEquals(Arrays.toString(expected), Arrays.toString(previousLines));
        assertArrayEquals(expected, previousLines);
    }

    @Test
    public void blamingSFThings()
    {
        Revision<SFThing> rev1 = new Revision<SFThing>(Arrays.asList(SFThing.ofText("test")));
        Revision<SFThing> rev2 = new Revision<SFThing>(Arrays.asList(
                SFThing.ofStartElement("strong"),
                SFThing.ofText("test"),
                SFThing.ofEndElement("strong")));

        List<Revision<SFThing>> history = Arrays.asList(rev1, rev2);

        List<Blame<SFThing>> expected = Arrays.asList(
                new Blame<SFThing>(SFThing.ofStartElement("strong"), rev2),
                new Blame<SFThing>(SFThing.ofText("test"), rev1),
                new Blame<SFThing>(SFThing.ofEndElement("strong"), rev2));

        List<Blame<SFThing>> blame = Blamer.blame(history);
        assertEquals(expected, blame);
    }
}
