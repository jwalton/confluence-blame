package com.atlassian.labs.fedex19.blame;
import java.util.List;


public final class Revision<T> implements Comparable<Revision<T>>
{
    private static int nextId = 1;
    private final int id = nextId++;

    final List<T> content;

    public Revision(List<T> content)
    {
        this.content = content;
    }

    public String toString()
    {
        return Integer.toString(id);
    }

    @Override
    public int compareTo(Revision<T> o)
    {
        if (id > o.id)
        {
            return 1;
        }
        else if (id < o.id)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}
