package com.atlassian.labs.fedex19.blame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.incava.util.diff.Diff;
import org.incava.util.diff.Difference;


public class Blamer
{
    public static <T> List<Blame<T>> blame(List<Revision<T>> historyForwards)
    {
        ArrayList<Revision<T>> history = new ArrayList<Revision<T>>(historyForwards);
        Collections.reverse(history);

        if (history.size() == 0)
        {
            return Collections.emptyList();
        }
        else
        {
            return blameRecursively(history);
        }
    }

    private static <T> List<Blame<T>> blameSingleton(List<Revision<T>> history)
    {
        assert history.size() == 1;

        List<Blame<T>> b = new ArrayList<Blame<T>>();

        for (T line : history.get(0).content)
        {
            b.add(new Blame<T>(line, history.get(0)));
        }

        return b;
    }

    private static <T> List<Blame<T>> blameRecursively(List<Revision<T>> history)
    {
        if (history.size() == 1)
        {
            return blameSingleton(history);
        }

        Diff<T> diff = new Diff<T>(history.get(0).content, history.get(1).content);

        List<Revision<T>> from = new ArrayList<Revision<T>>();

        /* Blame the previous revision lazily */
        List<Blame<T>> previous = null;

        for (int l : calculatePreviousLines(history.get(0).content.size(), diff.diff()))
        {
            if (l == -1)
            {
                from.add(history.get(0));
            }
            else
            {
                if (previous == null)
                {
                    /* Something came through from the previous revision */
                    previous = blameRecursively(history.subList(1, history.size()));
                }

                from.add(previous.get(l).getRevision());
            }
        }

        List<Blame<T>> b = new ArrayList<Blame<T>>();

        for (int j = 0; j < history.get(0).content.size(); j++)
        {
            b.add(new Blame<T>(history.get(0).content.get(j), from.get(j)));
        }

        return b;
    }

    public static int[] calculatePreviousLines(int currentSize, List<Difference> diff)
    {
        int[] previousLine = new int[currentSize];
        for (int i = 0; i < previousLine.length; i++)
        {
            previousLine[i] = -1;
        }

        int upto = 0;
        int offset = 0;

        for (Difference d : diff)
        {
            while (upto < d.getDeletedStart())
            {
                previousLine[upto] = upto + offset;
                upto++;
            }

            if (d.getDeletedEnd() >= 0)
            {
                upto = d.getDeletedEnd() + 1;
            }
            else
            {
                while (upto <= d.getDeletedEnd())
                {
                    previousLine[upto] = upto + offset;
                    upto++;
                }
            }

            int oc = 0;

            if (d.getAddedEnd() != Difference.NONE)
            {
                oc += d.getAddedEnd() - d.getAddedStart() +1;
            }

            if (d.getDeletedEnd() != Difference.NONE)
            {
                oc -= d.getDeletedEnd() - d.getDeletedStart() + 1;
            }

            offset += oc;
        }

        while (upto < previousLine.length)
        {
            previousLine[upto] = upto + offset;
            upto++;
        }

        return previousLine;
    }
}
