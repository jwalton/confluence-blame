package com.atlassian.labs.fedex19.blame;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;


public class ConfluenceHistory
{
    public List<Revision<SFThing>> revisions = new ArrayList<Revision<SFThing>>();
    public Map<Revision<SFThing>, RevDetails> revDetails = new HashMap<Revision<SFThing>, RevDetails>();

    public static ConfluenceHistory load(File f) throws IOException, ParserConfigurationException, SAXException
    {
        List<String> lines = FileUtils.readLines(f);

        if (lines.size() < 1)
        {
            throw new EOFException();
        }

        ConfluenceHistory ch = new ConfluenceHistory();

        int id = lines.size();

        for (String l : lines.subList(1, lines.size()))
        {
            String[] sa = l.split(", ");
            if (sa.length != 3)
            {
                throw new IOException("Bad line format");
            }

            File cf = new File(f.getParentFile(), sa[0] + ".storageformat");

            String contents = FileUtils.readFileToString(cf);
            Date when = new Date(sa[1]);
            String who = sa[2];

            ch.addRevision(contents, Integer.toString(id--), who, when);
        }

        return ch;
    }

    private void addRevision(Revision<SFThing> rev, RevDetails rd)
    {
        revisions.add(0, rev);
        revDetails.put(rev, rd);
    }

    public void addRevision(Revision<SFThing> rev, String revLabel, String who, Date when)
    {
        RevDetails rd = new RevDetails();
        rd.revLabel = revLabel;
        rd.who = who;
        rd.when = when;
        addRevision(rev, rd);
    }

    public void addRevision(String storageFormatContents, String revLabel, String who, Date when) throws IOException, ParserConfigurationException, SAXException
    {
        Revision<SFThing> rev = new Revision<SFThing>(StorageFormatTokens.byWordFragments().parse(storageFormatContents));

        addRevision(rev, revLabel, who, when);
    }
}
