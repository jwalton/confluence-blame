package com.atlassian.labs.fedex19.blame;

public class Blame<T>
{
    private final T content;
    private final Revision<T> revision;


    public Blame(T content, Revision<T> revision)
    {
        this.content = content;
        this.revision = revision;
    }

    public T getContent()
    {
        return content;
    }

    public Revision<T> getRevision()
    {
        return revision;
    }

    public String toString()
    {
        return content + "[" + revision + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Blame)
        {
            Blame<?> b = (Blame<?>) obj;
            return content.equals(b.content) && revision.equals(b.revision);
        }
        else
        {
            return false;
        }
    }
}
