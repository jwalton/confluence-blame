package com.atlassian.labs.fedex19.blame;
import java.util.Collections;
import java.util.Map;

import com.megginson.sax.XMLWriter;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;



public abstract class SFThing
{
    public static SFThing ofText(String s)
    {
        return new SFTextThing(s);
    }

    public static SFThing ofStartElement(String name)
    {
        return new SFStartElement(name, Collections.<String, String>emptyMap());
    }

    public static SFThing ofStartElement(String name, Map<String, String> attributes)
    {
        return new SFStartElement(name, attributes);
    }

    public static SFThing ofEndElement(String name)
    {
        return new SFEndElement(name);
    }

    abstract void write(XMLWriter ch) throws SAXException;
}

abstract class SFThingWithString extends SFThing
{
    final String s;

    public SFThingWithString(String s)
    {
        this.s = s;
    }

    @Override
    public int hashCode()
    {
        return s.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        return getClass().equals(obj.getClass()) && s.equals(((SFThingWithString) obj).s);
    }
}

class SFTextThing extends SFThingWithString
{
    public SFTextThing(String s)
    {
        super(s);
    }

    @Override
    public String toString()
    {
        return '"' + s + '"';
    }

    @Override
    void write(XMLWriter handler) throws SAXException
    {
        handler.characters(s);
    }
}

class SFStartElement extends SFThingWithString
{
    private final Map<String, String> attributes;

    public SFStartElement(String s, Map<String, String> attributes)
    {
        super(s);
        this.attributes = attributes;
    }

    @Override
    public String toString()
    {
        if (attributes.isEmpty())
        {
            return "start:" + s;
        }
        else
        {
            return "start:" + s + " " + attributes;
        }
    }

    @Override
    public int hashCode()
    {
        return super.hashCode() ^ attributes.hashCode();
    };

    @Override
    public boolean equals(Object obj)
    {
        return super.equals(obj) && attributes.equals(((SFStartElement)obj).attributes);
    }

    @Override
    void write(XMLWriter ch) throws SAXException
    {
        AttributesImpl attrs = new AttributesImpl();

        for (Map.Entry<String, String> e : attributes.entrySet())
        {
            attrs.addAttribute("", e.getKey(), "", "CDATA", e.getValue());
        }

        if (s.contains(":"))
        {
            ch.startElement("", s, "", attrs);
//            ch.startElement(StorageFormatTokens.XHTML_NS, "tt");
//            ch.characters("<" + s + ">");
//            ch.endElement(StorageFormatTokens.XHTML_NS, "tt");
        }
        else
        {
            ch.startElement(StorageFormatTokens.XHTML_NS, s, "", attrs);
        }
    }
}

class SFEndElement extends SFThingWithString
{
    public SFEndElement(String s)
    {
        super(s);
    }

    @Override
    public String toString()
    {
        return "end:" + s;
    }

    @Override
    void write(XMLWriter ch) throws SAXException
    {
        if (s.contains(":"))
        {
            ch.endElement(s);
//            ch.startElement(StorageFormatTokens.XHTML_NS, "tt");
//            ch.characters("</" + s + ">");
//            ch.endElement(StorageFormatTokens.XHTML_NS, "tt");
        }
        else
        {
            ch.endElement(StorageFormatTokens.XHTML_NS, s);
        }
    }
}
