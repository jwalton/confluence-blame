package com.atlassian.labs.fedex19.blame;

import java.awt.Color;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;

import com.megginson.sax.XMLWriter;

import org.xml.sax.SAXException;

public class BlameServlet extends HttpServlet
{
    protected PermissionManager permissionManager;
    protected PageManager pageManager;
    private Renderer renderer;

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setRenderer(Renderer renderer)
    {
        this.renderer = renderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String p = req.getParameter("pageId");
        if (p == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Provide a page ID");
            return;
        }

        long pageId;

        try
        {
            pageId = Long.parseLong(p);
        }
        catch (NumberFormatException nfe)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Page IDs are numeric");
            return;
        }

        AbstractPage ap = pageManager.getAbstractPage(pageId);

        if (ap == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
        }

        resp.setContentType("text/html");

//        PrintWriter w = resp.getWriter();

//        w.write("Current version: " + ap.getVersion() + "\n");

        List<VersionHistorySummary> summaries = pageManager.getVersionHistorySummaries(ap);

        ConfluenceHistory history = new ConfluenceHistory();

        for (VersionHistorySummary vhs : summaries)
        {
//            w.write(vhs.getVersion() + "," + vhs.getLastModifierName() + "," + vhs.getId() + "\n");
//
//            w.write(pageManager.getPage(vhs.getId()).getBodyAsString());

            try
            {
                history.addRevision(
                        pageManager.getPage(vhs.getId()).getBodyAsString(),
                        Integer.toString(vhs.getVersion()),
                        vhs.getLastModifierName(), vhs.getLastModificationDate());
            }
            catch (SAXException e)
            {
                throw new ServletException(e);
            }
            catch (ParserConfigurationException e)
            {
                throw new ServletException(e);
            }
        }

        // Not strictly XHTML now....
        resp.getWriter().println("<!DOCTYPE html><html><head><meta name='decorator' content='atl.general'/><title>Blame</title><meta charset='UTF-8'>");
        resp.getWriter().println("<body>");

        StringWriter sw = new StringWriter();

        try
        {
            write(sw, history);
        }
        catch (SAXException e)
        {
            throw new ServletException(e);
        }

        String s = sw.toString();

        /* Drop the XML declaration */
        int i = s.indexOf("?>");
        if (i >= 0)
        {
            s = s.substring(i + 2);
        }

        resp.getWriter().println(renderer.render(s, new DefaultConversionContext(ap.toPageContext())));

        resp.getWriter().println("</body></html>");
        resp.getWriter().close();
    }

//    static String marker = "HACKHACKHACK";

    void write(Writer w, final ConfluenceHistory history) throws SAXException, IOException
    {
        XMLWriter xw = new XMLWriter(w);

        OutputPage op = new OutputPage(xw){
            @Override
            public String titleFor(Revision<SFThing> r)
            {
                return history.revDetails.get(r).toString();
            }

            @Override
            public String randomColour(Revision<SFThing> r)
            {
                float hue = (float) Math.random();
                hue = new Random(history.revDetails.get(r).who.hashCode()).nextFloat() + (float) (Math.random() / 5);
                float saturation = 0.2f + (float) (Math.random() / 5);
                float brightness = 0.85f;

                return String.format("#%06X", 0xFFFFFF & Color.HSBtoRGB(hue, saturation, brightness));
            }

            @Override
            public String getRevLabel(Revision<SFThing> r)
            {
                return history.revDetails.get(r).revLabel;
            }
        };

        op.startHtmlBody();
//        xw.characters(marker);
        op.writeBlamed(Blamer.blame(history.revisions));
        op.writeRevisionKey(xw);
        op.endHtmlBody();

//        w.close();
    }
}
