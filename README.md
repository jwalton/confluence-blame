# Confluence Blame

Shows who wrote the current version of content.

# Examples

* CmdLineBlamer
* ShowBlamed

# Caveats

Standalone rather than a Confluence plugin: reads input as files
and prints output as storage-format-like XHTML.
